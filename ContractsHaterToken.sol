// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.9;

import "@openzeppelin/contracts/utils/Address.sol";
import "./IContractsHaterToken.sol";
import "./SimpleToken.sol";

contract ContractsHaterToken is
    IContractsHaterToken,
    SimpleToken("ContractsHaterToken", "CHT")
{
    mapping(address => bool) internal whitelist;

    function addToWhitelist(address candidate_) external onlyOwner {
        require(
            Address.isContract(candidate_),
            "ContractsHaterToken: the address is not a contract"
        );
        whitelist[candidate_] = true;
    }

    function removeFromWhitelist(address candidate_) external onlyOwner {
        whitelist[candidate_] = false;
    }

    function isWhitelisted(address candidate_) public view returns(bool) {
        return whitelist[candidate_];
    }
    
    function _beforeTokenTransfer(
        address from,
        address to,
        uint256 amount
    ) internal virtual override {
        require(
            from == address(0) ||
                to == address(0) ||
                !Address.isContract(to) ||
                isWhitelisted(to),
            "ContractsHaterToken: the contract must be whitelisted"
        );
    }
}

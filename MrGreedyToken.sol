// SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.9;

import "IMrGreedyToken.sol";
import "SimpleToken.sol";

contract MrGreedyToken is IMrGreedyToken, SimpleToken("MrGreedyToken", "MRG") {
    uint256 public constant oneToken_ = 10**6;

    address public immutable _treasury;

    constructor(address treasury_) {
        _treasury = treasury_;
    }

    function decimals() public view virtual override returns (uint8) {
        return 6;
    }

    function treasury() external view returns (address) {
        return _treasury;
    }

    function getResultingTransferAmount(uint256 amount_)
        public
        pure
        returns (uint256)
    {
        if (amount_ <= 10 * oneToken_) {
            return 0;
        }

        return amount_ - 10 * oneToken_;
    }

    function transfer(address to, uint256 amount)
        public
        virtual
        override
        returns (bool)
    {
        address owner = _msgSender();
        uint256 resultAmount = getResultingTransferAmount(amount);

        _transfer(owner, _treasury, amount - resultAmount);
        _transfer(owner, to, resultAmount);
        return true;
    }

    function transferFrom(
        address from,
        address to,
        uint256 amount
    ) public virtual override returns (bool) {
        address spender = _msgSender();
        uint256 resultAmount = getResultingTransferAmount(amount);

        _spendAllowance(from, spender, amount);
        _transfer(spender, _treasury, amount - resultAmount);
        _transfer(spender, to, resultAmount);
        return true;
    }
}
